<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'packwp_adaptat');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'h*9G0g$.yTwH7v&]lXlX=b]V;(ePxzEs`}qosuIMQ{!-:S_.r/NfeeL|45Qd|N!&');
define('SECURE_AUTH_KEY', 'IF%gatEhiGv#(d#l`zff5(q6 s$c1S=pb4skOQm&)n^(!4GsIv>`)^(7Qk*;-}+<');
define('LOGGED_IN_KEY', 'DF]S CExYQzhqS~vXfY&w m~f=%Xhcuou4H8t24R5T0/[:Wsz.fjIN+jnlC(|:j6');
define('NONCE_KEY', '@nU}/K(e1$En+B$ zk#ow3ziR[2eI@#JdHKPM:s-xAHvc)TU^%Cg%0xXGD&B4zn5');
define('AUTH_SALT', '6bgKm@CT}2.%hgVa|}9P<Cm8ye6H]8jl{G_5;qZW:@Am`]1d|(c3b[NMDjt{-_Un');
define('SECURE_AUTH_SALT', 'mA_QpD?2o?)20B*}=t^(Ow19:U2CwVs9vAS^-hQ%HQKF3afD}SxN|sOLU,ze=D7y');
define('LOGGED_IN_SALT', 'SH[Y$:DxB:|L.!V;$pGoAOnx_mm}>*BB,NH/]b^6vUH<Ky@nw4|XxdYbj4 *0=`7');
define('NONCE_SALT', 'r{LSP~o.RS1)_HgN~QZ6:OV)JZO].6CfV}a-hStQd<u*VewhXLkU8IDZi[$oIb;?');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

